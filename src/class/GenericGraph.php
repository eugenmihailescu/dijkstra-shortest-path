<?php
/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implements a generic graph
 *
 * @author Eugen Mihailescu
 * @version 0.1
 * @since 2015-04-12
 * @todo A memory-friendly version would implement a node names lookup table.
 *       The graph and node list will work with indexes rather than the original
 *       node name (which may be a very long string value)
 */
class GenericGraph {
	private $_directed;
	protected $_graph;
	protected $_nodes;
	protected $_infinity;
	protected $_lookup;
	/**
	 * Class constructor
	 *
	 * @param bool $directed
	 *        	Set it to true when the graph is directed/ordered, false otherwise
	 * @param number $infinity
	 *        	Specify the value for infinity (default PHP_INT_MAX)
	 */
	function __construct($directed = false, $infinity = PHP_INT_MAX) {
		$this->_graph = array ();
		$this->_nodes = array ();
		$this->_lookup = array ();
		$this->_directed = $directed;
		$this->_infinity = $infinity;
	}
	/**
	 *
	 * @param string $from
	 *        	The one end/node of the edge
	 * @param string $to
	 *        	The other end/node of the edge
	 * @return number Returns the edge length/weight or infinity when there is no edge between $from and $to
	 */
	protected function getEdgeLen($from, $to) {
		if (isset ( $this->_graph [$from] ) && isset ( $this->_graph [$from] [$to] ))
			return $this->_graph [$from] [$to];
		elseif (! $this->_directed && isset ( $this->_graph [$to] ) && isset ( $this->_graph [$to] [$from] ))
			return $this->_graph [$to] [$from];
		return $this->_infinity;
	}
	/**
	 * Returns the number of graph's edges
	 *
	 * @return number
	 */
	public function getEdgeCount() {
		$result = 0;
		foreach ( $this->_graph as $children )
			$result += count ( $children );
		return $result;
	}
	/**
	 * Add a new edge to the graph and adds the edge's ends to the node's list
	 *
	 * @param string $from
	 *        	The edge's source node
	 * @param string $to
	 *        	The edge's destination node
	 * @param comparable $weight
	 *        	The edge length (ie. cost, weight, whatever)
	 */
	public function addEdge($from, $to, $weight = 1) {
		$add2lookup = function ($n, &$l) {
			return ! isset ( $l [$n] ) && ($c = count ( $l ) + 1) && ($l [$n] = $c) ? $c : $l [$n];
		};
		
		// add the nodes names to the lookup table
		$from = $add2lookup ( $from, $this->_lookup );
		$to = $add2lookup ( $to, $this->_lookup );
		
		// add the edge to the graph
		! isset ( $this->_graph [$from] ) && $this->_graph [$from] = array ();
		$this->_graph [$from] [$to] = $weight;
		
		// add the nodes to the node list
		! in_array ( $from, $this->_nodes ) && $this->_nodes [] = $from;
		! in_array ( $to, $this->_nodes ) && $this->_nodes [] = $to;
		
		return true;
	}
	/**
	 * Returns the neighbours of a node
	 *
	 * @param string $node        	
	 * @return array
	 */
	protected function getAdiacentNodes($node) {
		$result = isset ( $this->_graph [$node] ) ? $this->_graph [$node] : array ();
		
		if (! $this->_directed)
			foreach ( $this->_graph as $n => $m )
				$n != $node && isset ( $m [$node] ) && $result [$n] = $m [$node];
		
		return $result;
	}
	/**
	 * Get the nodes of the graph
	 *
	 * @return array Returns the graph's nodes
	 */
	public function getNodes($real_names = false) {
		if ($real_names)
			return array_keys ( $this->_lookup );
		
		return $this->_nodes; // this is the lookup node ID
	}
	/**
	 * Returns the node names lookup table
	 *
	 * @return array
	 */
	public function getLookupTbl() {
		return $this->_lookup;
	}
	/**
	 * Save the whole graph to the external $filename
	 *
	 * @param string $filename        	
	 * @return Returns true if successfully saved, false otherwise
	 */
	public function saveToFile($filename) {
		$obj = array (
				'directed' => $this->_directed,
				'infinity' => $this->_infinity,
				'nodes' => $this->_nodes,
				'edges' => $this->_graph,
				'lookup' => $this->_lookup 
		);
		return file_put_contents ( $filename, json_encode ( $obj ) );
	}
	/**
	 * Load the entire graph (nodes/lookup table) from the specified $filename
	 *
	 * @param string $filename        	
	 * @return Returns true if successfully loaded, false otherwise
	 */
	public function loadFromFile($filename) {
		$obj = json_decode ( file_get_contents ( $filename ), true );
		if (false == $obj)
			return false;
		
		$this->_directed = $obj ['directed'];
		$this->_infinity = $obj ['infinity'];
		$this->_lookup = $obj ['lookup'];
		unset ( $obj ['lookup'] );
		$this->_nodes = $obj ['nodes'];
		unset ( $obj ['nodes'] );
		$this->_graph = $obj ['edges'];
		unset ( $obj ['edges'] );
		unset ( $obj );
		
		return true;
	}
}