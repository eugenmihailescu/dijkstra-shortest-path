<?php
/**
 * @example http://mynixworld.info/2015/04/11/dijkstra-shortest-path-algorithm/
 * @author Eugen Mihailescu
 * @since 2015-04-12
 * @license GPLv3
 */
$mem = memory_get_peak_usage ();

require_once dirname(__DIR__).'/class/Dijkstra.php';

$obj = new Dijkstra ();

$obj->addEdge ( 'A', 'B', 4 );
$obj->addEdge ( 'A', 'C', 1 );
$obj->addEdge ( 'A', 'D', 9 );
$obj->addEdge ( 'A', 'G', 7 );

$obj->addEdge ( 'C', 'E', 1 );
$obj->addEdge ( 'C', 'F', 3 );

$obj->addEdge ( 'D', 'G', 3 );

$obj->addEdge ( 'E', 'B', 7 );
$obj->addEdge ( 'E', 'F', 2 );

$obj->addEdge ( 'F', 'G', 2 );

$obj->addEdge ( 'H', 'F', 2 );
$obj->addEdge ( 'H', 'G', 4 );
$obj->addEdge ( 'H', 'D', 6 );

$path = $obj->getShortPath ( 'A', 'G' );

echo 'Example: the shortest path from A->G is:', PHP_EOL;
echo implode ( '->', array_keys ( $path ) ), ' (total weight=', array_sum ( $path ), ')', PHP_EOL;
printf ( 'Mem usage : %.2fMB' . PHP_EOL, (memory_get_peak_usage () - $mem) / 1048576 );

?>