<?php
/**
 * @author Eugen Mihailescu
 * @since 2015-04-12
 * @license GPLv3
 */
$mem = memory_get_peak_usage ();
require_once dirname ( __DIR__ ) . '/class/Dijkstra.php';

$path = isset ( $argv [1] ) ? $argv [1] : '/path-to/my-php-project/';
/**
 * Finds all PHP files recursively
 *
 * @return array
 */
function _scanDir($path, $recursive = false) {
	$result = array ();
	
	DIRECTORY_SEPARATOR != substr ( $path, - 1 ) && $path .= DIRECTORY_SEPARATOR;
	
	$dh = @opendir ( $path );
	if (false !== $dh) {
		while ( false !== ($file = readdir ( $dh )) ) {
			if ('.' == $file || '..' == $file)
				continue;
			
			$fullpath = $path . $file;
			
			if ($recursive && is_dir ( $fullpath ) && ! is_link ( $fullpath ))
				$result = array_merge ( $result, _scanDir ( $fullpath, $recursive ) );
			else if (preg_match ( '/.*\.php$/', $file ))
				$result [] = $fullpath;
		}
		closedir ( $dh );
	}
	return $result;
}
/**
 * Finds all PHP files specified in the include|require of the $filename
 *
 * @return array Returns an array of included files
 */
function getIncludeFiles($filename) {
	$buffer = file_get_contents ( $filename );
	if (preg_match_all ( '/^\s*(require|include)(_once)?\s+[^\'"]*[\'"](\w[\w\-\_\.]+)[\'"]/m', $buffer, $matches ))
		return $matches [3];
	
	return array ();
}

file_exists ( $path ) && ($dirlist = _scanDir ( $path, true )) || die ( 'No path specified.' . PHP_EOL . 'Usage: ' . PHP_EOL . "\tphp " . $argv [0] . ' [path-to-PHP-project]' . PHP_EOL . PHP_EOL );

$infinity = pow ( count ( $dirlist ), 2 );
$obj = new Dijkstra ( true, $infinity ); // a directed graph
                                         
// add graph edges (each file is an edge in graph)
foreach ( $dirlist as $filename ) {
	$includes = getIncludeFiles ( $filename );
	foreach ( $includes as $include )
		($filename = basename ( $filename )) && preg_match ( '/.*\.php$/', $include ) && $obj->addEdge ( $filename, $include );
	if (0 == count ( $includes ))
		$obj->addEdge ( $filename, '' );
}
unset ( $dirlist );

$startime = time ();

$matrix = array ();
foreach ( $obj->getNodes () as $i )
	foreach ( $obj->traverse ( $i ) as $k => $d )
		$matrix [$k] = (isset ( $matrix [$k] ) ? $matrix [$k] : 0) + $d ['w'];

asort ( $matrix, SORT_NUMERIC );

$lookup = $obj->getLookupTbl ();
echo PHP_EOL, 'Which are the most|least dependent files of your PHP project?', PHP_EOL;
echo str_repeat ( '-', 60 ), PHP_EOL;
printf ( '| Included | Filename%-38s|' . PHP_EOL, '' );
printf ( '| times(*) | %-46s|' . PHP_EOL, '' );
echo str_repeat ( '-', 60 ), PHP_EOL;

if (isset ( $lookup [''] ))
	unset ( $matrix [$lookup ['']] );
$min = min ( $matrix );
foreach ( $matrix as $file => $weight_sum ) {
	$file = basename ( array_search ( $file, $lookup ) );
	printf ( '| %-8d | %-46s|' . PHP_EOL, $weight_sum - $min, $file );
}
echo str_repeat ( '-', 60 ), PHP_EOL;

echo '(*) directly or by inheritence', PHP_EOL, PHP_EOL;
echo 'Tips: the smaller the number the more independent the file is (and vice-versa)', PHP_EOL;
printf ( 'Mem usage : %.2fMB' . PHP_EOL, (memory_get_peak_usage () - $mem) / 1048576 );
printf ( "\rFinished in %d sec (%d PHP files)" . PHP_EOL, time () - $startime, count ( $matrix ) );

?>