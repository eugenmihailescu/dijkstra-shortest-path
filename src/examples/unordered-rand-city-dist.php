<?php
/**
 * @author Eugen Mihailescu
 * @since 2015-04-12
 * @license GPLv3
 */
function genRandomCities($count = 50, $min_len = 3) {
	$alphabet = str_split ( 'AbCdEfGhIjKlMnOpRsTuVxYz' );
	$result = array ();
	$c = count ( $alphabet );
	for($i = 0; $i < $count; $i ++) {
		shuffle ( $alphabet );
		$n = '';
		while ( strlen ( $n ) < $min_len ) {
			$o = rand ( 0, $c );
			$n = implode ( '', array_slice ( $alphabet, $o, rand ( 2, $c - $o ) ) );
		}
		$result [] = $n;
	}
	return $result;
}
function genRandomPaths($cities) {
	$result = array ();
	foreach ( $cities as $from ) {
		$result [$from] = array ();
		foreach ( $cities as $to )
			$result [$from] [$to] = array (
					rand ( 1, 1000 ) 
			);
	}
	return $result;
}
require_once dirname ( __DIR__ ) . '/class/Dijkstra.php';

$graph_file = __DIR__ . DIRECTORY_SEPARATOR . 'res' . DIRECTORY_SEPARATOR . 'graph.json';

$obj = new Dijkstra ();

$start = microtime ( true );
$mem = memory_get_peak_usage ();
if (! (file_exists ( $graph_file ) && $obj->loadFromFile ( $graph_file ))) {
	
	$cities = genRandomCities ( isset ( $argv [1] ) ? $argv [1] : 100 ); // adapt this value to your PHP available memory
	$nodes = genRandomPaths ( $cities );
	
	$from_city = $cities [rand ( 0, count ( $cities ) - 1 )];
	$to_city = $cities [rand ( 0, count ( $cities ) - 1 )];
	
	// we remove the start/end city from the graph otherwise we would find probably that route as being the shortest
	// our goal is to use the Dijkstra algorithm such that we get at least 3 cities in the shortest route
	unset ( $nodes [$from_city] [$to_city] );
	unset ( $nodes [$to_city] [$from_city] );
	$init_dist = isset ( $nodes [$from_city] ) && isset ( $nodes [$from_city] [$to_city] ) ? $nodes [$from_city] [$to_city] [0] : (isset ( $nodes [$to_city] ) && isset ( $nodes [$to_city] [$from_city] ) ? $nodes [$to_city] [$from_city] [0] : PHP_INT_MAX);
	
	printf ( PHP_EOL . 'Generated %d random cities and %d routes between them (~ %d MB)' . PHP_EOL, count ( $cities ), pow ( count ( $cities ), 2 ), (memory_get_peak_usage () - $mem) / 1048576 );
	unset ( $cities );
	
	$mem = memory_get_peak_usage ();
	$start = microtime ( true );
	
	while ( ! empty ( $nodes ) ) {
		$from = key ( $nodes );
		$dest = current ( $nodes );
		
		while ( ! empty ( $dest ) ) {
			$to = key ( $dest );
			$values = current ( $dest );
			$obj->addEdge ( $from, $to, $values [0] );
			unset ( $dest [$to] );
		}
		unset ( $nodes [$from] );
	}
	
	$obj->saveToFile ( $graph_file );
} else {
	$nodes = $obj->getNodes ( true );
	printf ( PHP_EOL . 'Loaded the graph from the disk (%d cities)' . PHP_EOL, count ( $nodes ) );
	
	$from_city = $nodes [rand ( 0, count ( $nodes ) - 1 )];
	$to_city = $nodes [rand ( 0, count ( $nodes ) - 1 )];
	$init_dist = isset ( $nodes [$from_city] ) && isset ( $nodes [$from_city] [$to_city] ) ? $nodes [$from_city] [$to_city] [0] : (isset ( $nodes [$to_city] ) && isset ( $nodes [$to_city] [$from_city] ) ? $nodes [$to_city] [$from_city] [0] : PHP_INT_MAX);
}

printf ( PHP_EOL . 'Generated the map of cities (graph) in %.3f seconds; mem usage : %.2fMB' . PHP_EOL, microtime ( true ) - $start, (memory_get_peak_usage () - $mem) / 1048576 );

unset ( $nodes );

$start = microtime ( true );
$mem = memory_get_peak_usage ();

echo PHP_EOL, "Example:", PHP_EOL, " - the direct route from $from_city -> $to_city is, according to generated map, ", $init_dist, " km", PHP_EOL;
echo " - we removed the direct route from graph and tried to calculate an alternative shortest route between $from_city -> $to_city : ", PHP_EOL;

$t = $obj->getShortPath ( $from_city, $to_city );

echo "\t", implode ( ' -> ', array_keys ( $t ) ), sprintf ( ' (distance ~%.1f km)', end ( $t ) ), PHP_EOL;

reset ( $t );
$p = key ( $t );
$v = current ( $t );
next ( $t );
$i = 1;
foreach ( $t as $c => $m ) {
	$c != $p && printf ( "\t\t%d) %s -> %s => ~%.1f km" . PHP_EOL, $i ++, $p, $c, ($m - $v) );
	$p = $c;
	$v = $m;
}

printf ( PHP_EOL . 'Search finished in %.3f seconds; mem usage : %d bytes' . PHP_EOL, microtime ( true ) - $start, (memory_get_peak_usage () - $mem) );

?>