function ajaxRequest(url, params, callback, method) {
	'use strict';
	// Note: this should be used within the same domain (don't violate CORS)
	// On Firefox we only get a warning on Developer Console tool

	if (!method) {
		method = 'POST';// default
	}

	var xmlhttp, start = new Date();

	// http://en.wikipedia.org/wiki/XMLHttpRequest
	if (typeof XMLHttpRequest === 'undefined') {
		XMLHttpRequest = function() {
			try {
				return new ActiveXObject("Msxml2.XMLHTTP.6.0");
			} catch (e) {
			}
			try {
				return new ActiveXObject("Msxml2.XMLHTTP.3.0");
			} catch (e) {
			}
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
			}
			throw new Error("This browser does not support XMLHttpRequest.");
		};
	}

	xmlhttp = new window.XMLHttpRequest();

	if (callback) {
		/* jslint white: true */
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState >= 4)
				callback(xmlhttp, start);
		};
	}

	if (url) {
		try {

			xmlhttp.open(method, url, true);

			// Tells server that this call is made for ajax purposes.
			xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

			if (params && params.length > 0) {
				xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			}

			xmlhttp.send(params);
		} catch (err) {
			if (callback) {
				callback('JavaScript error: ' + err.message);
			}
		}
	} else {
		throw 'URL not specified!';
	}
	return xmlhttp;
}
function do_response(action, str) {
	switch (action) {
		case 'chg_city':
			document.getElementById('g_dist').innerHTML = str;
			break;
		case 'calc':
			var url, path = str.split('&'), parts, waypoints = [], cities = [], cities_pair = [], distances = [], total_dist = 0, g_dist, re, state, status;
			for (var i = 0; i < path.length; i += 1) {
				parts = path[i].split('=');
				cities.push(decodeURIComponent(parts[0]));
				distances.push(parts[1]);
				if (i > 0)
					cities_pair
							.push(cities[i - 1] + ' -&gt; ' + cities[i] + ' (~' + Math.round((parseInt(distances[i]) - parseInt(distances[i - 1])) / 1000) + 'km)');
				waypoints.push(parts[0] + '+Sweden');
			}
			if (1 == cities.length)
				cities_pair = cities;
			total_dist = Math.round(parseInt(distances[distances.length - 1])/1000, 1);
			re = /([\d]+(\.[\d]+)?)/;
			re = re.exec(document.getElementById('g_dist').innerHTML);
			if (total_dist <= parseInt(re[1])) {
				status = 'green';
				state = 'shorter';
			} else {
				status = 'red';
				state = 'longer';
			}
			state = '<span style="font-weight:bold;color:' + status + '">' + state + '</span>';
			var el = document.getElementById('calc_dist'), path_td = document.getElementById('path_td');
			if (el)
				if (cities.length > 0) {
					el.innerHTML = 'I found a ' + state + ' alternative route via ' + cities.length + ' cities of <span style="font-weight:bold;color:' + status + '">~' + total_dist + 'km</span>';
					if (path_td)
						path_td.innerHTML = '<ul><li>' + cities_pair.join('</li><li>') + '</li></ul>';
				} else
					el.innerHTML = 'no alternative route found';

			url = window.gm_url + "&origin=" + waypoints[0] + "+Sweden&destination=" + waypoints[waypoints.length - 1] + "+Sweden&waypoints=" + waypoints
					.join('|');
			document.getElementById('gm').src = url;
			upd_map();
			break;
	}
}

function do_action(action) {
	var from_city = document.getElementById('from_city'), to_city = document.getElementById('to_city'), from_city = from_city.options[from_city.selectedIndex].value;
	to_city = to_city.options[to_city.selectedIndex].value;
	ajaxRequest(location.href, 'action=' + action + '&from_city=' + from_city + '&to_city=' + to_city, function(xmlhttp) {
		do_response(action, xmlhttp.responseText);
	});
}
function upd_map() {
	var e = document.getElementById('trgm'), f = document.getElementById('tbgm'), g = document.getElementById('gm'), window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight, h = window_height - f.offsetTop - e.offsetTop - 30;
	if (g && f && g)
		g.style.height = h + "px";
}