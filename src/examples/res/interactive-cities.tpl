<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/interactive-cities.css">
<script type="text/javascript" src="js/interactive-cities.js"></script>
<link rel="stylesheet" type="text/css" href="interactive-cities.css">
<script type="text/javascript" src="interactive-cities.js"></script>
<title>Find the shortest route - Dijkstra's algorithm</title>
<script type="text/javascript">
	
</script>
</head>
<body>
	<div class="wrapper">
		<p style="text-align: center; font-size: 1.5em">This is an example
			of Dijkstra search algorithm usage</p>
		<p>
			Starting from <a
				href="http://mynixworld.info/2015/04/11/dijkstra-shortest-path-algorithm/"
				target="_blank">the post I wrote a while ago</a> about <a
				href="http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm"
				target="_blank">Dijkstra's algoritm</a> implmenetion in PHP I though
			that it would be interesting to write a small practical application
			for it.
		</p>
		<p>
			We have a list of <b> <?php echo $c;?> cities
			</b> from Sweden (and <?php echo $e;?> connections between them) <a
				href="http://mynixworld.info/2015/04/11/list-of-distances-between-cities/"
				target="_blank">gathered from Google</a> and we know the distance
			between <?php printf('%.1f%%',100*$e/$c/$c);?> of them. Therefore we know the Google's
			estimated distance between <?php printf('%.1f%%',100*$e/$c/$c);?> of any randomly chosen cities. The
			question is if we can find another alternative route that gives us a
			shorter distance than the one provided by Google.
		</p>
		<p>Dijkstra's algorithm helps us in determining the shortest path
			between two nodes/points on a graph/map and thus we may use it to solve our problem.</p>
		<table id='tbgm'
			style="padding: 10px; border: 1px solid #c0c0c0; border-radius: 10px; width: 100%">
			<tr>
				<td><label for="from_city">From:</label></td>
				<td><select id="from_city" onchange="do_action('chg_city');">
						<?php echo $cities_opts;?>
				</select></td>
				<td><label for="to_city">To:</label></td>
				<td><select id="to_city" onchange="do_action('chg_city');">
						<?php echo $cities_opts;?>
				</select></td>
				<td>Distance calculated by Google:</td>
				<td id="g_dist">
					<?php echo $google_dist;?>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="button"
					value="Find alternative route"
					style="cursor: pointer; color: white; background-color: #0596AF; border-radius: 4px;"
					onclick="do_action('calc');"></td>
				<td colspan="4" id="calc_dist"></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right; vertical-align: text-top;">The
					path calculated with<br>Dijkstra's algorithm goes through:
				</td>
				<td id="path_td" colspan="4"></td>
			</tr>
			<tr id="trgm">
				<td colspan="6"><iframe id="gm" src="<?php echo $gm_url;?>" style="border: 0; height: auto; width: 100%"></iframe></td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
		window.gm_url="<?php echo $gm_url_base;?>";
		upd_map();
	</script>
</body>
</html>
