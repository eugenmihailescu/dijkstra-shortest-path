<?php
require_once dirname ( __DIR__ ) . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'Dijkstra.php';

$graph_file = __DIR__ . DIRECTORY_SEPARATOR . 'res' . DIRECTORY_SEPARATOR . 'graph.json';
$gm_url_base = 'https://www.google.com/maps/embed/v1/directions?key=AIzaSyD9B-74HZUZYuwCsZQSKmwVCsbmbniiATs';

$nodes = json_decode ( file_get_contents ( __DIR__ . DIRECTORY_SEPARATOR . 'res' . DIRECTORY_SEPARATOR . 'cities.json' ), true );

$loaded = false;
$obj = new Dijkstra ();

if (! (file_exists ( $graph_file ) && $obj->loadFromFile ( $graph_file ))) {
	$cities = array_keys ( $nodes );
} else {
	if ($loaded = $obj->loadFromFile ( $graph_file )) {
		$cities = $obj->getNodes ( true );
		$e = $obj->getEdgeCount ();
	} else
		$cities = array_keys ( $nodes ); // eventually it is better smth. than anything
}
asort ( $cities );

$from_city = current ( $cities );
$to_city = $from_city;
$gm_url = $gm_url_base . '&amp;origin=' . urlencode ( "$from_city Sweden" ) . '&amp;destination=' . urlencode ( "$to_city Sweden" );

if (isset ( $_REQUEST ['action'] )) {
	$from_city = isset ( $_REQUEST ['from_city'] ) ? $_REQUEST ['from_city'] : null;
	$to_city = isset ( $_REQUEST ['to_city'] ) ? $_REQUEST ['to_city'] : null;
	if ('chg_city' == $_REQUEST ['action'] && $from_city && $to_city) {
		if (isset ( $nodes [$from_city] ) && isset ( $nodes [$from_city] [$to_city] ))
			$r = sprintf ( '%.1f km', $nodes [$from_city] [$to_city] [0] / 1000 );
		elseif (isset ( $nodes [$to_city] ) && isset ( $nodes [$to_city] [$from_city] ))
			$r = sprintf ( '%.1f km', $nodes [$to_city] [$from_city] [0] / 1000 );
		else
			$r = 'No data';
		die ( $r );
	}
	
	if ('calc' == $_REQUEST ['action'] && $from_city && $to_city) {
		
		// we remove the start/end city from the graph otherwise we would find probably that route as being the shortest
		// our goal is to use the Dijkstra algorithm such that we get at least 3 cities in the shortest route
		unset ( $nodes [$from_city] [$to_city] );
		unset ( $nodes [$to_city] [$from_city] );
		if (! $loaded) {
			$obj = new Dijkstra ();
			foreach ( $nodes as $from => $dest )
				foreach ( $dest as $to => $values )
					$e += $obj->addEdge ( $from, $to, $values [0] );
		}
		$obj->saveToFile ( $graph_file ); // for faster load next time
		$t = $obj->getShortPath ( $from_city, $to_city );
		die ( http_build_query ( $t ) );
	}
}

$cities_opts = '<option>' . implode ( '</option><option>', $cities ) . '</option>';

$google_dist = 'No data';
if (isset ( $nodes [$from_city] ) && isset ( $nodes [$from_city] [$to_city] ))
	$google_dist = sprintf ( '%.1f km', $nodes [$from_city] [$to_city] [0] / 1000 );

$c = count ( $cities );

include_once __DIR__ . DIRECTORY_SEPARATOR . 'res' . DIRECTORY_SEPARATOR . 'interactive-cities.tpl';

?>
