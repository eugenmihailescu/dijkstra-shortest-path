<?php
/**
 * @author Eugen Mihailescu
 * @since 2015-04-12
 * @license GPLv3
 */
$mem = memory_get_peak_usage ();
require_once dirname ( __DIR__ ) . '/class/Dijkstra.php';

$obj = new Dijkstra ();

$nodes = json_decode ( file_get_contents ( __DIR__ . DIRECTORY_SEPARATOR . 'res' . DIRECTORY_SEPARATOR . 'cities.json' ), true );
$cities = array_keys ( $nodes );

$from_city = isset ( $argv [1] ) ? $argv [1] : $cities [rand ( 0, rand ( 1, count ( $cities ) - 1 ) )];
$to_city = isset ( $argv [2] ) ? $argv [2] : $cities [rand ( 0, rand ( 1, count ( $cities ) - 1 ) )];
$google_dist = isset ( $nodes [$from_city] ) && isset ( $nodes [$from_city] [$to_city] ) ? $nodes [$from_city] [$to_city] [0] : (isset ( $nodes [$to_city] ) && isset ( $nodes [$to_city] [$from_city] ) ? $nodes [$to_city] [$from_city] [0] : PHP_INT_MAX);

// we remove the start/end city from the graph otherwise we would find probably that route as being the shortest
// our goal is to use the Dijkstra algorithm such that we get at least 3 cities in the shortest route
unset ( $nodes [$from_city] [$to_city] );
unset ( $nodes [$to_city] [$from_city] );

$start = microtime ( true );

foreach ( $nodes as $from => $dest )
	foreach ( $dest as $to => $values )
		$obj->addEdge ( $from, $to, $values [0] );

echo "Example:", PHP_EOL, " - the direct route from $from_city->$to_city is, according to Google Map, ", intval ( $google_dist / 1000 ), " km", PHP_EOL;
echo " - we removed the direct route from graph and tried to calculate an alternative shortest route between $from_city->$to_city : ", PHP_EOL;

$t = $obj->getShortPath ( $from_city, $to_city );

if (false !== $t) {
	echo "\t", implode ( '->', array_keys ( $t ) ), sprintf ( ' (distance ~%.1f km)', end ( $t ) / 1000 ), PHP_EOL;
	
	reset ( $t );
	$p = key ( $t );
	$v = current ( $t );
	next ( $t );
	foreach ( $t as $c => $m ) {
		$c != $p && printf ( "\t\t%s -> %s => ~%.1f km" . PHP_EOL, $p, $c, ($m - $v) / 1000 );
		$p = $c;
		$v = $m;
	}
} else
	echo "\t\tno route found";
printf ( PHP_EOL . 'Finished in %.3f seconds; mem usage : %.2fMB' . PHP_EOL, microtime ( true ) - $start, (memory_get_peak_usage () - $mem) / 1048576 );

?>